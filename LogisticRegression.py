import numpy as np
import cupy as cp


class LogisticRegression:

    def __init__(self, threshold=.5):
        self.__w = None
        self.__b = None
        self.__threshold = threshold

    def get_threshold(self):
        return self.__threshold

    def set_threshold(self, threshold):
        self.__threshold = threshold

    def cost_function(self, y_true, y_pred):
        xp = cp.get_array_module(y_true)
        y_pred = xp.asarray(y_pred)
        n = len(y_true)
        losses = -1 * (y_true * xp.log(y_pred)) - ((1 - y_true) * xp.log(1 - y_pred))
        cost = losses.sum() / n
        return cost

    def __logit(self, x):
        xp = cp.get_array_module(x)
        return 1/(1+xp.exp(-1*x))
    
    def fit(self, X, y, n_iterations=1000, lr=.001):
        xp = cp.get_array_module(X)
        n_samples, n_features = X.shape
        self.__b = xp.zeros(1)
        self.__w = xp.zeros(n_features)
        y = xp.asarray(y)
        for _ in range(n_iterations):
            self.__w -= lr*(self.__logit(self.__w.dot(X.transpose() + self.__b)) - y).dot(X) / n_samples
            self.__b -= lr*(self.__logit(self.__w.dot(X.transpose() + self.__b)) - y).sum() / n_samples
    

    def predict(self, X):
        xp = cp.get_array_module(X)
        y = self.__logit(self.__w.dot(X.transpose()) + self.__b)
        return y ,xp.where(y < self.__threshold, 0, 1)
